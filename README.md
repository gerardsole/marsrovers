# Mars Rovers

This solution contains the implementation for the Rovers landing 
using a terminal/unit tests, but also a remote interface to
interact with it.



## First exercise

I've decided to try a command pattern, where Commands are the actions to be done for the given object.

A Command would wrap a Robot instance and execute the corresponding method. You can find 3 commands:
    
* Move
* TiltRight
* TiltLeft

Then, the robot and the commands list are send to the Mission object which is in charge of executing 
them and store the robot positions.

I've assumed that a robot cannot go outside of the bounds of the map. To do so, the mission provides
an interface to validate the next positions.

The string sequence parsing has been done using 
regex (I should say I'm not used to them, but they fitted pretty well).


To run the application issue the following command

```
./gradlew console
```

Then a prompt like the following one will appear, add the whole sequence and submit it.

```
> Task :console
Insert mission sequence. Ex: 5 5 1 2 N LMLMLMLMM 3 3 E MMRMMRMRRM
```

## Extension Exercise

To solve this part, a MicroHttpServer has been built on top of a TCP Socket.
I could have stick just to use a raw TCP socket and design my own exchange 
protocol, but I wanted to search which was the very minimum set of required 
HTTP Headers.

The implementation can be found inside the package `com.newrelic.marsrovers.api`.

Implementation is tested using OkHTTP as a client running on JUnit.

In fact, the network interface can be tested using cURL for example or any other HTTP client that
sends the standard HTTP Request Headers.

### Run the server

Start the server by running the following command. 
Wait to see the port binding (Listening at port 3000)

```gradle
./gradlew server
```

### Interact with the server

I've tested the API with OkHTTP, cURL and Postman. Here you can find the cURL commands to run.

* POST /mission
    
    **Creates a new mission** with the given map constraints, the robots and it's movements.
    
    Returns the final location for all the robots
    
    ```
    curl -d "5 5 1 2 N LMLMLMLMM 3 3 E MMRMMRMRRM" -X POST http://localhost:3000/mission
    ```

* POST /mission/robot

    Lands a new robot on a **previously created mission**. Post the initial position
    along with its movements
    
    Returns the final position for this robot

    ```
    curl -d "5 5 N LL" -X POST http://localhost:3000/mission/robot
    ```
   
* PUT /mission/robot

    Updates a already landed robot from a **previous created mission**. Post just the
    location of the robot, and it's movements.
    
    Returns the final position for this robot.

    ```
    curl -d "5 5 N LL" -X PUT http://localhost:3000/mission/robot
    ```

* GET /mission

    Gets the positions for all the robots that have been landed. The output order is sequential
    with the input order of the robots
    
    ```
    curl http://localhost:3000/mission
    ```


## Testing & Coverage

The application (both parts) have been tested using JUnit5, and Mockito for the Mocks. 

Request tests are done using OkHTTP library from Square also running in JUnit5

97 Test Cases have been built and a coverage of ~95% is achieved according to clover.

Run the following gradle command to test and generate the report

```
./gradlew cloverGenerateReport
```

Two reports are generated if you want to open them this are the locations:

* Tests (passing and failing): 
```
build/reports/tests/test/index.html
```

* Coverage: 
```
build/reports/clover/html/index.html
```
