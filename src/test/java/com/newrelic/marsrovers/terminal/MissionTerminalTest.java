package com.newrelic.marsrovers.terminal;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class MissionTerminalTest {

    @Test
    void run() throws Exception {
        MissionTerminal missionTerminal = new MissionTerminal("5 5 1 2 N LMLMLMLMM 3 3 E MMRMMRMRRM");
        assertEquals("1 3 N 5 1 E", missionTerminal.run());
    }

    @Test
    void run2() throws Exception {
        MissionTerminal missionTerminal = new MissionTerminal("1 1 1 1 N MMMMM");
        assertEquals("1 1 N", missionTerminal.run());
    }
}
