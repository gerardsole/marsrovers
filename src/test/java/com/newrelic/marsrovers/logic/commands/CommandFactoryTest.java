package com.newrelic.marsrovers.logic.commands;

import com.newrelic.marsrovers.logic.models.Robot;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.MethodSource;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.Mockito.mock;

class CommandFactoryTest {

    @DisplayName("getCommand()")
    @ParameterizedTest(name = "{1} should return command of type {0}")
    @MethodSource("getTestValues")
    void getCommand(Class<Command> expected, String commandLetter) {
        Robot robotMock = mock(Robot.class);
        Command command = new CommandFactory(robotMock).getCommand(commandLetter);

        assertEquals(expected, command.getClass());
    }

    @DisplayName("getCommandForInvalidValue()")
    @ParameterizedTest(name = "{0} should return null")
    @CsvSource(value={"unexpected", "F"})
    void getCommandForInvalidValue(String commandLetter) {
        Robot robotMock = mock(Robot.class);
        Command command = new CommandFactory(robotMock).getCommand(commandLetter);

        assertNull(command);
    }

    private static Stream<Object[]> getTestValues() {
        List<Object[]> data = new ArrayList<>();
        data.add(new Object[]{TiltLeftCommand.class, "L"});
        data.add(new Object[]{TiltLeftCommand.class, "l"});
        data.add(new Object[]{MoveCommand.class, "M"});
        data.add(new Object[]{MoveCommand.class, "m"});
        data.add(new Object[]{TiltRightCommand.class, "R"});
        data.add(new Object[]{TiltRightCommand.class, "r"});
        return data.stream();
    }

}
