package com.newrelic.marsrovers.logic.commands;

import com.newrelic.marsrovers.logic.models.Robot;

import org.junit.jupiter.api.Test;

import static org.mockito.Mockito.*;

class TiltRightCommandTest {

    @Test
    void execute() {
        Robot robotMock = mock(Robot.class);
        Command command = new TiltRightCommand(robotMock);
        command.execute();

        verify(robotMock, times(1)).tiltRight();
    }
}
