package com.newrelic.marsrovers.logic;

import com.newrelic.marsrovers.logic.commands.Command;
import com.newrelic.marsrovers.logic.commands.CommandFactory;
import com.newrelic.marsrovers.logic.exceptions.MissionException;
import com.newrelic.marsrovers.logic.models.Position;
import com.newrelic.marsrovers.logic.models.Robot;

import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class MissionTest {

    @Test
    void addRobot() throws MissionException {
        Robot robotMock = mock(Robot.class);
        when(robotMock.getPosition()).thenReturn(new Position(0, 0));
        Mission mission = createMission();
        mission.addRobot(robotMock);

        verify(robotMock, times(1)).addPositionChecker(mission);
    }

    @Test
    void addRobotInvalidPosition() {
        assertThrows(MissionException.class, () -> {
            Robot robotMock = mock(Robot.class);
            when(robotMock.getPosition()).thenReturn(new Position(-1, -1));
            Mission mission = createMission();
            mission.addRobot(robotMock);
        });
    }

    @Test
    void runCommands() throws MissionException {
        Robot robotMock = mock(Robot.class);
        when(robotMock.getPosition()).thenReturn(new Position(0, 0));
        CommandFactory factory = new CommandFactory(robotMock);
        List<Command> commands = Arrays.asList(
                factory.getCommand("L"),
                factory.getCommand("R"),
                factory.getCommand("M"),
                null);
        Mission mission = createMission();
        mission.addRobot(robotMock);
        mission.runCommands(commands);

        verify(robotMock, times(1)).tiltLeft();
        verify(robotMock, times(1)).tiltRight();
        verify(robotMock, times(1)).move();
    }

    @Test
    void getRobotsPosition() throws MissionException {
        Robot robotMock1 = mock(Robot.class);
        when(robotMock1.getPosition()).thenReturn(new Position(1, 1));
        when(robotMock1.getPositionWithHeading()).thenReturn("1 1 N");
        Robot robotMock2 = mock(Robot.class);
        when(robotMock2.getPosition()).thenReturn(new Position(2, 2));
        when(robotMock2.getPositionWithHeading()).thenReturn("2 2 S");
        Mission mission = createMission();
        mission.addRobot(robotMock1);
        mission.addRobot(robotMock2);

        assertEquals("1 1 N 2 2 S", mission.getRobotsPosition());
        verify(robotMock1, times(1)).addPositionChecker(mission);
        verify(robotMock2, times(1)).addPositionChecker(mission);
        verify(robotMock1, times(1)).getPositionWithHeading();
        verify(robotMock2, times(1)).getPositionWithHeading();
    }

    @Test
    void validPosition() {
        Mission mission = createMission();

        assertTrue(mission.validPosition(new Position(0, 0)));
        assertFalse(mission.validPosition(new Position(6, 0)));
        assertFalse(mission.validPosition(new Position(0, 6)));
        assertFalse(mission.validPosition(new Position(-1, 0)));
        assertFalse(mission.validPosition(new Position(0, -1)));
    }

    private Mission createMission() {
        return new Mission(5, 5);
    }
}
