package com.newrelic.marsrovers.logic.util;

import com.newrelic.marsrovers.logic.models.Cardinal;
import com.newrelic.marsrovers.logic.models.Position;
import com.newrelic.marsrovers.logic.models.RobotSequence;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Stream;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import static org.junit.jupiter.api.Assertions.*;

class SequenceParserTest {

    @Test
    void getMapXY() {
        SequenceParser sequenceParser = new SequenceParser("5 5 1 2 N LMLMLMLMM 3 3 E MMRMMRMRRM");

        assertDoesNotThrow(() -> assertArrayEquals(new int[]{5, 5}, sequenceParser.getMapXandY()));
    }

    @Test
    void getMapXYWithException(){
        assertThrows(Exception.class, () -> new SequenceParser("5 S").getMapXandY());
    }

    @DisplayName("getRobotSequences()")
    @ParameterizedTest(name = "{1} produces a robot sequence")
    @MethodSource("getRobotTestSequences")
    void getRobotSequences(List<RobotSequence> expected, String sequence) {
        SequenceParser sequenceParser = new SequenceParser(sequence);

        assertIterableEquals(expected, sequenceParser.getRobotSequences());
    }

    private static Stream<Object[]> getRobotTestSequences() {
        List<Object[]> testData = new ArrayList<>();
        List<RobotSequence> expectedRobotSequence = Arrays.asList(
                new RobotSequence(new Position(1, 2), Cardinal.NORTH, "LMLMLMLMM"),
                new RobotSequence(new Position(3, 3), Cardinal.EAST, "MMRMMRMRRM"));
        testData.add(new Object[]{expectedRobotSequence, "5 5 1 2 N LMLMLMLMM 3 3 E MMRMMRMRRM"});

        expectedRobotSequence = Collections.singletonList(
                new RobotSequence(new Position(3, 3), Cardinal.SOUTH, "RLMRLM"));
        testData.add(new Object[]{expectedRobotSequence, "3 3 S RLMRLM"});
        return testData.stream();
    }
}
