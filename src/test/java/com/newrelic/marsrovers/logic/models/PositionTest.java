package com.newrelic.marsrovers.logic.models;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;


class PositionTest {

    @Test
    void fromPosition() {
        Position position = createPosition();
        Position newPosition = position.copy();

        assertEquals(position, newPosition);
        assertNotEquals(position.hashCode(), newPosition.hashCode());
    }

    @Test
    void getX() {
        assertEquals(5, createPosition().getX());
    }

    @Test
    void getY() {
        assertEquals(5, createPosition().getY());
    }

    @Test
    void equalsWhenSamePosition() {
        Position expectedPosition = createPosition();
        Position position = createPosition(5, 5);

        assertEquals(expectedPosition, position);
        assertNotEquals(expectedPosition.hashCode(), position.hashCode());
    }

    @Test
    void equalsWhenSameInstance() {
        Position expectedPosition = createPosition();

        assertEquals(expectedPosition, expectedPosition);
    }

    @Test
    void notEqualsWhenDifferentPosition() {
        Position expectedPosition = createPosition();
        Position position = createPosition(0, 0);

        assertNotEquals(expectedPosition, position);
    }

    @Test
    void notEqualsWhenDifferentTypes() {
        Position expectedPosition = createPosition();

        assertNotEquals(expectedPosition, "");
    }

    @Test
    void notEqualsWhenNullPosition() {
        Position expectedPosition = createPosition();

        assertNotEquals(expectedPosition, null);
    }

    @Test
    void up() {
        Position position = createPosition();
        position.up();

        assertEquals(6, position.getY());
    }

    @Test
    void down() {
        Position position = createPosition();
        position.down();

        assertEquals(4, position.getY());
    }

    @Test
    void right() {
        Position position = createPosition();
        position.right();

        assertEquals(6, position.getX());
    }

    @Test
    void left() {
        Position position = createPosition();
        position.left();

        assertEquals(4, position.getX());
    }

    private Position createPosition() {
        return createPosition(5, 5);
    }

    private Position createPosition(int x, int y) {
        return new Position(x, y);
    }
}
