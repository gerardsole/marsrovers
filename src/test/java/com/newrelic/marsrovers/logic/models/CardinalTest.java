package com.newrelic.marsrovers.logic.models;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import static org.junit.jupiter.api.Assertions.assertEquals;

class CardinalTest {

    @DisplayName("abbreviation()")
    @ParameterizedTest(name = "{0} should be {1}")
    @CsvSource({"NORTH, N", "EAST, E", "SOUTH, S", "WEST, W"})
    void abbreviation(Cardinal cardinal, String expected) {
        assertEquals(expected, cardinal.abbreviation());
    }

    @DisplayName("left()")
    @ParameterizedTest(name = "{0} should be {1}")
    @CsvSource({"NORTH, WEST", "WEST, SOUTH", "SOUTH, EAST", "EAST, NORTH"})
    void left(Cardinal cardinal, Cardinal expected) {
        assertEquals(expected, cardinal.left());
    }

    @DisplayName("right()")
    @ParameterizedTest(name = "{0} should be {1}")
    @CsvSource({"NORTH, EAST", "EAST, SOUTH", "SOUTH, WEST", "WEST, NORTH"})
    void right(Cardinal cardinal, Cardinal expected) {
        assertEquals(expected, cardinal.right());
    }

    @DisplayName("fromAbbreviation()")
    @ParameterizedTest(name = "{0} should be {1}")
    @CsvSource({"n, NORTH", "N, NORTH", "e, EAST", "E, EAST", "s, SOUTH",
            "S, SOUTH", "w, WEST", "W, WEST"})
    void fromAbbreviation(String abbr, Cardinal expected) {
        assertEquals(expected, Cardinal.fromAbbreviation(abbr));
    }
}
