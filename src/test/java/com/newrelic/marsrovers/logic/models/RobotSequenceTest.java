package com.newrelic.marsrovers.logic.models;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class RobotSequenceTest {

    @Test
    void getPosition() {
        Position expected = new Position(0, 0);

        assertSame(expected, createRobotSequence(expected).getPosition());
    }

    @Test
    void getHeading() {
        assertSame(Cardinal.NORTH, createRobotSequence().getHeading());
    }

    @Test
    void getCommands() {
        String[] expected = new String[]{"L", "R", "M", "L", "R", "M"};

        assertArrayEquals(expected, createRobotSequence().getCommands());
    }

    @Test
    void equalsWhenSameRobotSequence() {
        RobotSequence expected = createRobotSequence();
        RobotSequence newRobotSequence = createRobotSequence();

        assertEquals(expected, newRobotSequence);
        assertNotEquals(expected.hashCode(), newRobotSequence.hashCode());
    }

    @Test
    void equalsWhenSameInstance() {
        RobotSequence expected = createRobotSequence();

        assertEquals(expected, expected);
    }

    @Test
    void notEqualsWhenDifferentRobotSequence() {
        assertNotEquals(createRobotSequence(), createRobotSequence(new Position(1, 1)));
    }

    @Test
    void notEqualsWhenDifferentTypes() {
        assertNotEquals(createRobotSequence(), "");
    }

    @Test
    void notEqualsWhenNullPosition() {
        assertNotEquals(createRobotSequence(), null);
    }

    private RobotSequence createRobotSequence() {
        return createRobotSequence(new Position(0, 0));
    }

    private RobotSequence createRobotSequence(Position position) {
        return createRobotSequence(position, Cardinal.NORTH);
    }

    private RobotSequence createRobotSequence(Position position, Cardinal cardinal) {
        return new RobotSequence(position, cardinal, "LRMLRM");
    }
}
