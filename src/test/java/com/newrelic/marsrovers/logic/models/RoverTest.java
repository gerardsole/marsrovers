package com.newrelic.marsrovers.logic.models;

import com.newrelic.marsrovers.logic.PositionChecker;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.mockito.Mockito.*;

class RoverTest {

    @Test
    void moveWhenHeadingNorthAndCheckerReturnsTrue() {
        Position nextPositionMock = mock(Position.class);
        moveRobotHelper(nextPositionMock, Cardinal.NORTH, true);

        verify(nextPositionMock, times(1)).up();
    }

    @Test
    void moveWhenHeadingEastAndCheckerReturnsTrue() {
        Position nextPositionMock = mock(Position.class);
        moveRobotHelper(nextPositionMock, Cardinal.EAST, true);

        verify(nextPositionMock, times(1)).right();
    }

    @Test
    void moveWhenHeadingSouthAndCheckerReturnsTrue() {
        Position nextPositionMock = mock(Position.class);
        moveRobotHelper(nextPositionMock, Cardinal.SOUTH, true);

        verify(nextPositionMock, times(1)).down();
    }

    @Test
    void moveWhenHeadingWestAndCheckerReturnsTrue() {
        Position nextPositionMock = mock(Position.class);
        moveRobotHelper(nextPositionMock, Cardinal.WEST, true);

        verify(nextPositionMock, times(1)).left();
    }

    @Test
    void moveWhenHeadingNorthAndCheckerReturnsFalse() {
        Position nextPositionMock = mock(Position.class);
        moveRobotHelper(nextPositionMock, Cardinal.NORTH, false);

        verify(nextPositionMock, times(1)).up();
    }

    @Test
    void moveWhenHeadingEastAndCheckerReturnsFalse() {
        Position nextPositionMock = mock(Position.class);
        moveRobotHelper(nextPositionMock, Cardinal.EAST, false);

        verify(nextPositionMock, times(1)).right();
    }

    @Test
    void moveWhenHeadingSouthAndCheckerReturnsFalse() {
        Position nextPositionMock = mock(Position.class);
        moveRobotHelper(nextPositionMock, Cardinal.SOUTH, false);

        verify(nextPositionMock, times(1)).down();
    }

    @Test
    void moveWhenHeadingWestAndCheckerReturnsFalse() {
        Position nextPositionMock = mock(Position.class);
        moveRobotHelper(nextPositionMock, Cardinal.WEST, false);

        verify(nextPositionMock, times(1)).left();
    }

    @Test
    void tiltLeft() {
        Robot robot = createRobot();
        robot.tiltLeft();

        assertEquals(Cardinal.WEST, robot.getHeading());
    }

    @Test
    void tiltRight() {
        Robot robot = createRobot();
        robot.tiltRight();

        assertEquals(Cardinal.EAST, robot.getHeading());
    }

    @Test
    void getHeading() {
        assertEquals(Cardinal.NORTH, createRobot().getHeading());
    }

    @Test
    void getPosition() {
        Position expected = new Position(0, 0);
        Robot robot = new Rover(expected, Cardinal.NORTH);

        assertEquals(expected, robot.getPosition());
        assertSame(expected, robot.getPosition());
    }

    @Test
    void printPositionWithHeading() {
        assertEquals("0 0 N", createRobot().getPositionWithHeading());
    }

    @Test
    void addPositionChecker() {
        PositionChecker positionCheckerMock = mock(PositionChecker.class);
        Robot robot = createRobot();
        robot.addPositionChecker(positionCheckerMock);
        robot.move();

        verify(positionCheckerMock, times(1)).validPosition(any(Position.class));
    }

    private Robot createRobot() {
        return new Rover(new Position(0, 0), Cardinal.NORTH);
    }

    private void moveRobotHelper(Position nextPosition, Cardinal heading, boolean checkerResult) {
        PositionChecker positionCheckerMock = mock(PositionChecker.class);
        when(positionCheckerMock.validPosition(nextPosition)).thenReturn(checkerResult);
        Position positionMock = mock(Position.class);
        when(positionMock.getX()).thenReturn(1);
        when(positionMock.getY()).thenReturn(1);
        when(positionMock.copy()).thenReturn(nextPosition);
        Robot robot = new Rover(positionMock, heading);
        robot.addPositionChecker(positionCheckerMock);
        robot.move();

        verify(positionMock, times(1)).copy();
        verify(positionCheckerMock, times(1)).validPosition(nextPosition);
        if (checkerResult) {
            assertEquals(nextPosition, robot.getPosition());
        } else {
            assertEquals(positionMock, robot.getPosition());
        }
    }
}
