package com.newrelic.marsrovers.api;

import java.io.IOException;

import okhttp3.*;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;


class MissionServerTest {

    private static final String HOST = "http://localhost:3000";
    private MissionServer missionServer;
    private OkHttpClient client;

    @BeforeEach
    void setUp() {
        missionServer = new MissionServer();
        missionServer.start(10);
        client = new OkHttpClient();
    }

    @AfterEach
    void tearDown() {
        missionServer.stop();
    }

    @Test
    void postNewMission() throws IOException {
        Response response = createNewMission();

        assertEquals("1 3 N 5 1 E", response.body().string().trim());
        assertEquals(200, response.code());
    }

    @Test
    void postNewMissionThenAddNewRobot() throws IOException {
        postNewMission();
        Response response = httpClientPost("/mission/robot", "1 1 N MMM");

        assertEquals("1 4 N", response.body().string().trim());
        assertEquals(200, response.code());
    }

    @Test
    void postNewMissionThenUpdateRobot() throws IOException {
        postNewMission();
        Response response = httpClientPut("5 1 RRMMMMM");

        assertEquals("0 1 W", response.body().string().trim());
        assertEquals(200, response.code());
    }

    @Test
    void failOnPostNewMissionThenUpdateRobot() throws IOException {
        postNewMission();
        Response response = httpClientPut("0 0 RRMMMMM");

        assertEquals("Robot could not be updated", response.body().string().trim());
        assertEquals(500, response.code());
    }

    @Test
    void failOnPostNewMissionThenLandNewRobot() throws IOException {
        postNewMission();
        Response response = httpClientPost("/mission/robot", "RRMMMMM");

        assertEquals("Robot could not be landed", response.body().string().trim());
        assertEquals(500, response.code());
    }

    @Test
    void getRobotsPosition() throws IOException {
        createNewMission();
        Response response = httpClientGet();

        assertEquals("1 3 N 5 1 E", response.body().string().trim());
        assertEquals(200, response.code());
    }

    @Test
    void serverSideErrors() throws IOException {
        Response response = httpClientPost("/missions", "test");
        assertEquals("Path /missions is not implemented", response.body().string().trim());
        assertEquals(500, response.code());

        response = httpClientGet();
        assertEquals("Mission is not initialized", response.body().string().trim());
        assertEquals(500, response.code());

        response = httpClientDelete();
        assertEquals("Request method is not supported", response.body().string().trim());
        assertEquals(500, response.code());
    }

    private Response createNewMission() throws IOException {
        return httpClientPost("/mission", "5 5 1 2 N LMLMLMLMM 3 3 E MMRMMRMRRM");
    }

    private Response httpClientPut(String body) throws IOException {
        Request request = new Request.Builder()
                .url(String.format("%s%s", HOST, "/mission/robot"))
                .put(RequestBody.create(MediaType.parse("text"), body))
                .build();
        return execute(request);
    }

    private Response httpClientPost(String path, String body) throws IOException {
        Request request = new Request.Builder()
                .url(String.format("%s%s", HOST, path))
                .post(RequestBody.create(MediaType.parse("text"), body))
                .build();
        return execute(request);
    }

    private Response httpClientGet() throws IOException {
        Request request = new Request.Builder()
                .url(String.format("%s%s", HOST, "/mission"))
                .get()
                .build();
        return execute(request);
    }

    private Response httpClientDelete() throws IOException {
        Request request = new Request.Builder()
                .url(String.format("%s%s", HOST, "/mission"))
                .delete()
                .build();
        return execute(request);
    }

    private Response execute(Request request) throws IOException {
        return client.newCall(request).execute();
    }
}
