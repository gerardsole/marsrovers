package com.newrelic.marsrovers.api;

import com.newrelic.marsrovers.logic.exceptions.MissionException;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MissionApiTest {

    @Test
    void createMissionWithSequence() {
        assertDoesNotThrow(() -> {
            MissionApi missionApi = new MissionApi();

            assertEquals("1 3 N 5 1 E",
                    missionApi.createMissionWithSequence("5 5 1 2 N LMLMLMLMM 3 3 E MMRMMRMRRM"));
        });
    }

    @Test
    void createMissionWithSequenceFailForInvalidSequence() {
        assertThrows(MissionException.class, () -> {
            MissionApi missionApi = new MissionApi();
            missionApi.createMissionWithSequence("5 X 1 2 N LMLMLMLMM 3 3 E MMRMMRMRRM");
        });
    }

    @Test
    void updateRobotWithSequence() {
        assertDoesNotThrow(() -> {
            MissionApi missionApi = createMissionApi();
            assertEquals("1 5 N", missionApi.updateRobotWithSequence("1 3 MM"));
            assertEquals("1 5 N 5 1 E", missionApi.getRobotsPosition());
        });
    }

    @Test
    void updateRobotWithSequenceFailForMissingRobot() {
        assertThrows(MissionException.class, () -> {
            MissionApi missionApi = createMissionApi();
            missionApi.updateRobotWithSequence("0 0 MM");
        });
    }

    @Test
    void getRobotsPosition() {
        assertDoesNotThrow(() -> {
            MissionApi missionApi = createMissionApi();
            assertEquals("1 3 N 5 1 E", missionApi.getRobotsPosition());
        });
    }

    @Test
    void addRobotWithSequence() {
        assertDoesNotThrow(() -> {
            MissionApi missionApi = createMissionApi();
            assertEquals("3 3 E", missionApi.addRobotWithSequence("1 3 E MM"));
            assertEquals("1 3 N 5 1 E 3 3 E", missionApi.getRobotsPosition());
        });
    }

    @Test
    void addRobotWithSequenceFailForInvalidSequence() {
        assertThrows(MissionException.class, () -> {
            MissionApi missionApi = createMissionApi();
            missionApi.addRobotWithSequence("1 X E MM");
        });
    }

    @Test
    void checkMissionInitialized() {
        assertThrows(MissionException.class, () -> {
            MissionApi missionApi = new MissionApi();
            missionApi.updateRobotWithSequence("1 1 LRLRM");
        });
    }

    private MissionApi createMissionApi() throws MissionException {
        MissionApi missionApi = new MissionApi();
        missionApi.createMissionWithSequence("5 5 1 2 N LMLMLMLMM 3 3 E MMRMMRMRRM");
        return missionApi;
    }
}
