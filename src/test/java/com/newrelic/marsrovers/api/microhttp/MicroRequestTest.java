package com.newrelic.marsrovers.api.microhttp;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MicroRequestTest {

    @Test
    void whenRequestGetBodyIsNull() {
        assertDoesNotThrow(() -> assertBody(null, new StringReader(requestGetExample())));
    }

    @Test
    void whenRequestPostBodyHasValue() {
        assertDoesNotThrow(() -> {
            String expected = "123";
            assertBody(expected, new StringReader(requestPostExample(expected)));
        });
    }

    private void assertBody(String expected, Reader reader) throws IOException {
        MicroRequest microRequest = new MicroRequest(new BufferedReader(reader));
        assertEquals(expected, microRequest.body());
    }

    @Test
    void uri() {
        assertDoesNotThrow(() -> {
            Reader reader = new StringReader(requestGetExample());
            MicroRequest request = new MicroRequest(new BufferedReader(reader));
            assertEquals("/mission/robot", request.uri());
        });
    }

    @Test
    void method() {
        assertDoesNotThrow(() -> {
            Reader reader = new StringReader(requestGetExample());
            MicroRequest request = new MicroRequest(new BufferedReader(reader));
            assertEquals(MicroHttpMethod.GET, request.method());
        });
    }

    private String requestGetExample() {
        StringBuilder sb = new StringBuilder();
        sb.append("GET /mission/robot HTTP/1.1\n");
        sb.append("Host: localhost:3000\n");
        sb.append("\n\n");
        sb.append("body");
        return sb.toString();
    }

    private String requestPostExample(String data) {
        StringBuilder sb = new StringBuilder();
        sb.append("POST /mission/robot HTTP/1.1\n");
        sb.append("Host: localhost:3000\n");
        sb.append(String.format("Content-Length: %d\n", data.length()));
        sb.append("Content-Type: application/x-www-form-urlencoded\n");
        sb.append("\n");
        sb.append(data);
        return sb.toString();
    }
}
