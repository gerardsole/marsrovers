package com.newrelic.marsrovers.api.microhttp;

import java.io.BufferedWriter;
import java.io.IOException;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

class MicroResponseTest {

    @Test
    void send() {
        assertDoesNotThrow(() -> {
            String data = "test";
            BufferedWriter bufferedWriterMock = mock(BufferedWriter.class);
            MicroResponse response = new MicroResponse(bufferedWriterMock);
            response.send("test");

            verify(bufferedWriterMock).write("HTTP/1.0 200 OK\r\n");
            assertWrite(data, bufferedWriterMock);
            verify(bufferedWriterMock, times(7)).write(anyString());
        });
    }

    @Test
    void error() {
        assertDoesNotThrow(() -> {
            String data = "exception";
            BufferedWriter bufferedWriterMock = mock(BufferedWriter.class);
            MicroResponse response = new MicroResponse(bufferedWriterMock);
            response.error(new Exception(data));

            verify(bufferedWriterMock).write("HTTP/1.0 500 Internal Server Error\r\n");
            assertWrite(data, bufferedWriterMock);
            verify(bufferedWriterMock, times(7)).write(anyString());
        });
    }

    private void assertWrite(String data, BufferedWriter bufferedWriterMock) throws IOException {
        data += "\n";
        verify(bufferedWriterMock).write("Content-Type: text/html; charset=utf-8\r\n");
        verify(bufferedWriterMock).write(String.format("Content-Length: %d\r\n", data.length()));
        verify(bufferedWriterMock).write("Server: MissionServer\r\n");
        verify(bufferedWriterMock).write("Connection: Close\r\n");
        verify(bufferedWriterMock).write("\r\n");
        verify(bufferedWriterMock).write(data);
        verify(bufferedWriterMock, times(7)).write(anyString());
    }
}
