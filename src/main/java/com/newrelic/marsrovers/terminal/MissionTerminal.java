package com.newrelic.marsrovers.terminal;

import com.newrelic.marsrovers.logic.Mission;
import com.newrelic.marsrovers.logic.commands.CommandFactory;
import com.newrelic.marsrovers.logic.exceptions.MissionException;
import com.newrelic.marsrovers.logic.models.Robot;
import com.newrelic.marsrovers.logic.models.RobotSequence;
import com.newrelic.marsrovers.logic.models.Rover;
import com.newrelic.marsrovers.logic.util.SequenceParser;

class MissionTerminal {
    private final SequenceParser sequence;

    MissionTerminal(String sequence) {
        this.sequence = new SequenceParser(sequence);
    }

    String run() throws MissionException {
        int[] mapBounds = sequence.getMapXandY();
        Mission mission = new Mission(mapBounds[0], mapBounds[1]);
        for (RobotSequence robotSequence : this.sequence.getRobotSequences()) {
            Robot robot = new Rover(robotSequence.getPosition(), robotSequence.getHeading());
            mission.addRobot(robot);
            CommandFactory factory = new CommandFactory(robot);
            mission.runCommands(factory.getCommands(robotSequence.getCommands()));
        }

        return mission.getRobotsPosition();
    }
}
