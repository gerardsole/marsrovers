package com.newrelic.marsrovers.terminal;

import java.util.Scanner;

public class ConsoleMain {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Insert mission sequence. Ex: 5 5 1 2 N LMLMLMLMM 3 3 E MMRMMRMRRM");
        String sequence = scanner.nextLine();
        try {
            System.out.println(new MissionTerminal(sequence).run());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
