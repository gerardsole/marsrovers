package com.newrelic.marsrovers.api;

import com.newrelic.marsrovers.api.microhttp.MicroHttpServer;
import com.newrelic.marsrovers.api.microhttp.MicroRequest;
import com.newrelic.marsrovers.logic.exceptions.MissionException;

import static java.lang.String.format;

public class MissionServer {
    private final MissionApi missionApi;
    private MicroHttpServer server;

    MissionServer() {
        this.missionApi = new MissionApi();
    }

    public static void main(String[] args) {
        new MissionServer().start();
    }

    void start() {
        start(5000);
    }

    void start(int timeout){
        server = new MicroHttpServer(3000, timeout);
        server.incomingRequest(this::processRequest);
    }

    private String processRequest(MicroRequest microRequest) throws MissionException {
        switch (microRequest.method()) {
            case POST:
                return processPostRequest(microRequest);
            case GET:
                return missionApi.getRobotsPosition();
            case PUT:
                return missionApi.updateRobotWithSequence(microRequest.body());
            default:
                throw new MissionException("Request method is not supported");
        }
    }

    private String processPostRequest(MicroRequest microRequest) throws MissionException {
        switch (microRequest.uri()) {
            case "/mission":
                return missionApi.createMissionWithSequence(microRequest.body());
            case "/mission/robot":
                return missionApi.addRobotWithSequence(microRequest.body());
            default:
                break;
        }
        throw new MissionException(format("Path %s is not implemented", microRequest.uri()));
    }

    void stop() {
        server.stop();
    }
}
