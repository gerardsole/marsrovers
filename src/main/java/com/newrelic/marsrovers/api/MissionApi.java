package com.newrelic.marsrovers.api;

import com.newrelic.marsrovers.logic.Mission;
import com.newrelic.marsrovers.logic.commands.CommandFactory;
import com.newrelic.marsrovers.logic.exceptions.MissionException;
import com.newrelic.marsrovers.logic.models.Robot;
import com.newrelic.marsrovers.logic.models.RobotSequence;
import com.newrelic.marsrovers.logic.models.Rover;
import com.newrelic.marsrovers.logic.util.SequenceParser;

import java.util.Objects;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

class MissionApi {

    private Mission mission;

    /**
     * Creates a new mission using the given sequence.
     *
     * @param sequence string containing the map boundaries, and robots
     * @return returns the location for all the robots
     * @throws MissionException if mission raised an error or inconsistent state
     */
    String createMissionWithSequence(String sequence) throws MissionException {
        SequenceParser sequenceParser = new SequenceParser(sequence);

        int[] boundaries = sequenceParser.getMapXandY();
        mission = new Mission(boundaries[0], boundaries[1]);

        return positionResult(sequence, this::addRobot)
                .collect(Collectors.joining(" "));
    }

    private String addRobot(RobotSequence robotSequence) {
        Robot robot = new Rover(robotSequence.getPosition(), robotSequence.getHeading());
        try {
            mission.addRobot(robot);
            CommandFactory factory = new CommandFactory(robot);
            mission.runCommands(factory.getCommands(robotSequence.getCommands()));

            return robot.getPositionWithHeading();
        } catch (MissionException ex) {
            return null;
        }
    }

    /**
     * From the supplied sequence, extract the robot position and it's commands.
     * Find the robot on the mission and send the new actions.
     *
     * @param sequence string representation of a robot and its commands
     * @return returns the location of the given robot
     * @throws MissionException if mission raised an error or inconsistent state
     */
    String updateRobotWithSequence(String sequence) throws MissionException {
        checkMissionInitialized();

        return positionResult(sequence, robotSequence -> {
            Robot robot = mission.getRobot(robotSequence.getPosition());
            if (Objects.nonNull(robot)) {
                CommandFactory factory = new CommandFactory(robot);
                mission.runCommands(factory.getCommands(robotSequence.getCommands()));

                return robot.getPositionWithHeading();
            }
            return null;
        }).filter(Objects::nonNull)
                .findFirst()
                .orElseThrow(() -> new MissionException("Robot could not be updated"));
    }

    /**
     * Queries the mission to find the position of all the robots.
     *
     * @return the position representation of all the robots
     * @throws MissionException if mission raised an error or inconsistent state
     */
    String getRobotsPosition() throws MissionException {
        checkMissionInitialized();
        return mission.getRobotsPosition();
    }

    /**
     * Adds a new robot into the mission using the supplied sequence.
     *
     * @param sequence contains the position, heading and actions to be done.
     * @return the position representation of the given robot
     * @throws MissionException if mission raised an error or inconsistent state
     */
    String addRobotWithSequence(String sequence) throws MissionException {
        checkMissionInitialized();

        return positionResult(sequence, this::addRobot)
                .findFirst()
                .orElseThrow(() -> new MissionException("Robot could not be landed"));
    }

    private void checkMissionInitialized() throws MissionException {
        if (Objects.isNull(mission)) {
            throw new MissionException("Mission is not initialized");
        }
    }

    private Stream<String> positionResult(String sequence,
                                          Function<RobotSequence, String> callback) {
        SequenceParser sequenceParser = new SequenceParser(sequence);
        return sequenceParser.getRobotSequences().stream()
                .map(callback)
                .filter(Objects::nonNull);
    }
}
