package com.newrelic.marsrovers.api.microhttp;

import com.newrelic.marsrovers.logic.exceptions.MissionException;

@FunctionalInterface
public interface RequestFunction<MicroRequest, R> {
    R apply(MicroRequest microRequest) throws MissionException;
}
