package com.newrelic.marsrovers.api.microhttp;

public enum MicroHttpMethod {
    POST, GET, PUT, HEAD, DELETE
}
