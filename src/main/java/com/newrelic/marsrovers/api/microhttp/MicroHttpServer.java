package com.newrelic.marsrovers.api.microhttp;

import com.newrelic.marsrovers.logic.exceptions.MissionException;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.ServerSocket;
import java.net.Socket;

public class MicroHttpServer {

    private final int port;
    private final int timeout;
    private Thread serverThread;

    public MicroHttpServer(int port, int timeout) {
        this.port = port;
        this.timeout = timeout;
    }

    /**
     * Start listening the socket if this method is called. When a new request is received,
     * it is decoded as a MicroRequest and sent to the callback. With the result, write to the
     * socket using the MicroResponse.
     *
     * @param callback supplied function that implements the request handling
     */
    public void incomingRequest(RequestFunction<MicroRequest, String> callback) {
        serverThread = new Thread(() -> {
            try (ServerSocket serverSocket = new ServerSocket(port)) {
                serverSocket.setSoTimeout(timeout);
                System.out.printf("Listening on port %d\n", port);
                while (!Thread.currentThread().isInterrupted()) {
                    listenSocket(callback, serverSocket);
                }
            } catch (IOException exception) {
                exception.printStackTrace();
            }
        });
        serverThread.start();
    }

    /**
     * Stop the server thread gracefully.
     */
    public void stop() {
        System.out.println("Stopping MicroHttpServer");
        serverThread.interrupt();
        try {
            serverThread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void listenSocket(RequestFunction<MicroRequest, String> callback,
                              ServerSocket socket) {
        try (Socket clientSocket = socket.accept();
             BufferedReader in = new BufferedReader(
                     new InputStreamReader(clientSocket.getInputStream()));
             BufferedWriter out = new BufferedWriter(
                     new OutputStreamWriter(clientSocket.getOutputStream()))
        ) {
            System.out.println("Connection received");
            MicroResponse microResponse = new MicroResponse(out);
            try {
                MicroRequest microRequest = new MicroRequest(in);
                microResponse.send(callback.apply(microRequest));
            } catch (MissionException ex) {
                microResponse.error(ex);
            }
        } catch (IOException ignored) {
        }
    }
}
