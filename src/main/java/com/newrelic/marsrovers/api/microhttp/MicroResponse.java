package com.newrelic.marsrovers.api.microhttp;

import java.io.BufferedWriter;
import java.io.IOException;

class MicroResponse {
    private final BufferedWriter out;

    MicroResponse(BufferedWriter out) {
        this.out = out;
    }

    void send(String data) throws IOException {
        out.write("HTTP/1.0 200 OK\r\n");
        writeStringResponse(data);
    }

    void error(Exception exception) throws IOException {
        out.write("HTTP/1.0 500 Internal Server Error\r\n");
        writeStringResponse(exception.getMessage());
    }

    private void writeStringResponse(String data) throws IOException {
        data += "\n";
        out.write("Content-Type: text/html; charset=utf-8\r\n");
        out.write(String.format("Content-Length: %d\r\n", data.length()));
        out.write("Server: MissionServer\r\n");
        out.write("Connection: Close\r\n");
        out.write("\r\n");
        out.write(data);
    }
}
