package com.newrelic.marsrovers.api.microhttp;

import java.io.BufferedReader;
import java.io.IOException;

public class MicroRequest {
    private int contentLength;
    private String body;
    private MicroHttpMethod method;
    private String uri;

    MicroRequest(BufferedReader reader) throws IOException {
        parse(reader);
    }

    public String body() {
        return body;
    }

    public String uri() {
        return uri;
    }

    public MicroHttpMethod method() {
        return method;
    }

    private void parse(BufferedReader reader) throws IOException {
        readRequestLine(reader.readLine());
        searchContentLength(reader);
        readBody(reader);
    }

    private void searchContentLength(BufferedReader reader) throws IOException {
        String line;
        while ((line = reader.readLine()) != null) {
            if (line.equals("")) {
                break;
            }
            if (line.contains("Content-Length")) {
                this.contentLength = Integer.parseInt(line.replaceAll("Content-Length: ", ""));
            }
        }
    }

    private void readRequestLine(String requestLine) {
        String[] splitted = requestLine.split(" ");
        method = MicroHttpMethod.valueOf(splitted[0]);
        uri = splitted[1];
    }

    private void readBody(BufferedReader reader) throws IOException {
        if (methodHasBody()) {
            char[] buffer = new char[contentLength];
            reader.read(buffer, 0, contentLength);
            body = new String(buffer);
        }
    }

    private boolean methodHasBody(){
        return (method == MicroHttpMethod.POST || method == MicroHttpMethod.PUT);
    }
}
