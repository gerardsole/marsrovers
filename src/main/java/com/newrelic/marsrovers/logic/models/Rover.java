package com.newrelic.marsrovers.logic.models;

import com.newrelic.marsrovers.logic.PositionChecker;

import java.util.Objects;


public class Rover implements Robot {

    private Cardinal heading;
    private Position position;
    private PositionChecker positionChecker;

    public Rover(Position position, Cardinal heading) {
        this.position = position;
        this.heading = heading;
    }

    /**
     * When move is called, it will generate the next position according to
     * the current heading.
     * Then, this position is validated against the checker. If valid, is stored.
     */
    public final void move() {
        Position nextPosition = getNextPosition();
        if (Objects.nonNull(positionChecker) && positionChecker.validPosition(nextPosition)) {
            position = nextPosition;
        }
    }

    public final void tiltLeft() {
        heading = heading.left();
    }

    public final void tiltRight() {
        heading = heading.right();
    }

    public Cardinal getHeading() {
        return heading;
    }

    public Position getPosition() {
        return position;
    }

    /**
     * Returns the position and heading of the robot.
     *
     * @return "x y h"
     */
    public String getPositionWithHeading() {
        return String.format("%d %d %s",
                position.getX(), position.getY(), heading.abbreviation());
    }

    @Override
    public void addPositionChecker(PositionChecker positionChecker) {
        this.positionChecker = positionChecker;
    }

    private Position getNextPosition() {
        Position newPosition = position.copy();
        switch (heading) {
            case NORTH:
                newPosition.up();
                break;
            case SOUTH:
                newPosition.down();
                break;
            case EAST:
                newPosition.right();
                break;
            case WEST:
                newPosition.left();
                break;
        }

        return newPosition;
    }
}
