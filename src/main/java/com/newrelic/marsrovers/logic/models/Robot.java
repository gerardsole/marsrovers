package com.newrelic.marsrovers.logic.models;

import com.newrelic.marsrovers.logic.PositionChecker;

public interface Robot {

    void move();

    void tiltLeft();

    void tiltRight();

    Cardinal getHeading();

    Position getPosition();

    String getPositionWithHeading();

    void addPositionChecker(PositionChecker movementChecker);
}
