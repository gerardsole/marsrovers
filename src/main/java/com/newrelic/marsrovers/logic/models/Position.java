package com.newrelic.marsrovers.logic.models;

public class Position {

    private int x;
    private int y;

    public Position(int x, int y) {
        this.x = x;
        this.y = y;
    }

    Position copy() {
        return new Position(getX(), getY());
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        } else if (obj == null || getClass() != obj.getClass()) {
            return false;
        } else {
            Position position = (Position) obj;
            return x == position.x && y == position.y;
        }
    }

    void up() {
        y++;
    }

    void down() {
        y--;
    }

    void right() {
        x++;
    }

    void left() {
        x--;
    }
}
