package com.newrelic.marsrovers.logic.models;

import java.util.Arrays;

public enum Cardinal {
    NORTH("N"),
    EAST("E"),
    SOUTH("S"),
    WEST("W");

    private final String abbreviation;
    private static Cardinal[] cardinals = values();


    Cardinal(final String abbreviation) {
        this.abbreviation = abbreviation;
    }

    public String abbreviation() {
        return this.abbreviation;
    }

    /**
     * By making use of the ordinal method in enums, we can compute which is the
     * cardinal on the left.
     *
     * @return the new Cardinal on the left
     */
    public Cardinal left() {
        int leftIdx = this.ordinal() - 1;
        leftIdx = (leftIdx < 0) ? (cardinals.length - 1) : leftIdx;

        return cardinals[leftIdx];
    }

    /**
     * By making use of the ordinal method in enums, we can compute which is the
     * cardinal on the right based on modulus 4.
     *
     * @return the new Cardinal on the right
     */
    public Cardinal right() {
        return cardinals[(this.ordinal() + 1) % 4];
    }

    /**
     * Get the Cardinal value from an abbreviation.
     *
     * @param abbr the abbreviation representation of the cardinal
     * @return returns a cardinal
     */
    public static Cardinal fromAbbreviation(final String abbr) {
        return Arrays.stream(cardinals)
                .filter(c -> c.abbreviation.equals(abbr.toUpperCase()))
                .findAny()
                .orElse(null);
    }
}
