package com.newrelic.marsrovers.logic.models;

public class RobotSequence {

    private final Position position;
    private final Cardinal heading;
    private final String commands;

    /**
     * Constructs a RobotSequence object from which a Robot can be build
     * and commands executed.
     *
     * @param position the position of the robot
     * @param heading the appropriate Cardinal enum
     * @param commands a string containing the commands sequences
     */
    public RobotSequence(final Position position, final Cardinal heading, final String commands) {
        this.position = position;
        this.heading = heading;
        this.commands = commands;
    }

    public Position getPosition() {
        return position;
    }

    public Cardinal getHeading() {
        return heading;
    }

    public String[] getCommands() {
        return commands.split("(?!^)");
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        } else if (obj == null || getClass() != obj.getClass()) {
            return false;
        } else {
            return equalMembers((RobotSequence) obj);
        }
    }

    private boolean equalMembers(RobotSequence that){
        return position.equals(that.position)
                && heading.equals(that.heading)
                && commands.equals(that.commands);
    }
}
