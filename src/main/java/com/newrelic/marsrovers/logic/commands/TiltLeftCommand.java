package com.newrelic.marsrovers.logic.commands;

import com.newrelic.marsrovers.logic.models.Robot;

public class TiltLeftCommand implements Command {

    private final Robot robot;

    TiltLeftCommand(Robot robot) {
        this.robot = robot;
    }

    @Override
    public void execute() {
        robot.tiltLeft();
    }
}
