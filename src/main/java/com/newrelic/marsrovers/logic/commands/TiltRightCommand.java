package com.newrelic.marsrovers.logic.commands;

import com.newrelic.marsrovers.logic.models.Robot;

public class TiltRightCommand implements Command {

    private final Robot robot;

    TiltRightCommand(Robot robot) {
        this.robot = robot;
    }

    @Override
    public void execute() {
        robot.tiltRight();
    }
}
