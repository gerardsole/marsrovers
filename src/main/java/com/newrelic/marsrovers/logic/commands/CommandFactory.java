package com.newrelic.marsrovers.logic.commands;

import com.newrelic.marsrovers.logic.models.Robot;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class CommandFactory {

    private final Robot robot;

    public CommandFactory(final Robot robot) {
        this.robot = robot;
    }

    /**
     * Returns the Command instance of the corresponding to the command letter
     * that will be executed into the supplied robot.
     *
     * @param action   Letter corresponding to an action (L: tilt left, R: tilt right, M: move)
     * @return  the command action to be executed
     */
    public Command getCommand(final String action) {
        switch (action.toUpperCase()) {
            case "R":
                return new TiltRightCommand(this.robot);
            case "L":
                return new TiltLeftCommand(this.robot);
            case "M":
                return new MoveCommand(this.robot);
            default:
                return null;
        }
    }

    /**
     * Returns all the commands that match with the supplied command sequence.
     *
     * @param commands a string array containing all the commands to built
     * @return  a list of the actions to be executed
     */
    public List<Command> getCommands(String[] commands) {
        return Arrays
                .stream(commands)
                .map(this::getCommand)
                .filter(Objects::nonNull)
                .collect(Collectors.toList());
    }
}
