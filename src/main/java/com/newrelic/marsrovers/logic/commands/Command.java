package com.newrelic.marsrovers.logic.commands;

public interface Command {
    void execute();
}
