package com.newrelic.marsrovers.logic.commands;

import com.newrelic.marsrovers.logic.models.Robot;

public class MoveCommand implements Command {

    private final Robot robot;

    MoveCommand(Robot robot) {
        this.robot = robot;
    }

    @Override
    public void execute() {
        robot.move();
    }
}
