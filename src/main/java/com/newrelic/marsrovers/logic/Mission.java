package com.newrelic.marsrovers.logic;

import com.newrelic.marsrovers.logic.commands.Command;
import com.newrelic.marsrovers.logic.exceptions.MissionException;
import com.newrelic.marsrovers.logic.models.Position;
import com.newrelic.marsrovers.logic.models.Robot;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class Mission implements PositionChecker {

    private final int mapBoundX;
    private final int mapBoundY;
    private final int mapInitX;
    private final int mapInitY;

    private final List<Robot> robots = new ArrayList<>();

    private Mission(int initX, int initY, int boundX, int boundY) {
        this.mapInitX = initX;
        this.mapInitY = initY;
        this.mapBoundX = boundX;
        this.mapBoundY = boundY;
    }

    public Mission(int boundX, int boundY) {
        this(0, 0, boundX, boundY);
    }

    /**
     * Adds a Robot to the mission. First of all, validate if the requested position
     * is valid. If so, attach to the robot a PositionChecker and store it.
     *
     * @param robot a robot which is added into the mission
     * @throws MissionException throws an exception if position is outside of the bounds
     */
    public void addRobot(Robot robot) throws MissionException {
        if (!validPosition(robot.getPosition())){
            throw new MissionException("Robot can not land outside of the map");
        }
        robot.addPositionChecker(this);
        this.robots.add(robot);
    }

    /**
     * Execute all the command objects that on its underlay will run the robot actions.
     *
     * @param commands a list of commands
     */
    public void runCommands(List<Command> commands) {
        commands.stream()
                .filter(Objects::nonNull)
                .forEach(Command::execute);
    }

    /**
     * Returns the robot positions in the appropriate format.
     * They are sequential to the insertion order, and contain the position and the heading.
     *
     * @return a string with the positions
     */
    public String getRobotsPosition() {
        return this.robots
                .stream()
                .map(Robot::getPositionWithHeading)
                .collect(Collectors.joining(" "));
    }

    /**
     * Validates a given position.
     *
     * @param position position to validate
     * @return true if valid, false otherwise
     */
    @Override
    public boolean validPosition(Position position) {
        return (position.getX() >= mapInitX && position.getX() <= mapBoundX)
                && position.getY() >= mapInitY && position.getY() <= mapBoundY;
    }

    /**
     * Get a robot from the given position.
     *
     * @param position the position to search for a robot
     * @return a robot if is found, null otherwise
     */
    public Robot getRobot(Position position) {
        return robots
                .stream()
                .filter(r -> r.getPosition().equals(position))
                .findFirst()
                .orElse(null);
    }
}
