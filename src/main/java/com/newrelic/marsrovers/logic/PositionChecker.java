package com.newrelic.marsrovers.logic;

import com.newrelic.marsrovers.logic.models.Position;

public interface PositionChecker {
    boolean validPosition(Position position);
}
