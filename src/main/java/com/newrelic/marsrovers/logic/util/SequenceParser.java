package com.newrelic.marsrovers.logic.util;

import com.newrelic.marsrovers.logic.exceptions.MissionException;
import com.newrelic.marsrovers.logic.models.Cardinal;
import com.newrelic.marsrovers.logic.models.Position;
import com.newrelic.marsrovers.logic.models.RobotSequence;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SequenceParser {
    private static final String COMMANDS_GROUP = "commands";
    private static final String HEADING_GROUP = "heading";
    private static final String POSITION_GROUP = "position";
    private static final String MAP_GROUP = "map";

    private static final Pattern MAP_PATTERN = Pattern.compile(
            "^(?<map>\\d \\d)", Pattern.CASE_INSENSITIVE);
    private static final Pattern ROBOT_PATTERN = Pattern.compile(
            "(?<position>(\\d \\d) (?<heading>[NESW])?\\s?(?<commands>[a-z]+))+",
            Pattern.CASE_INSENSITIVE);

    private final String sequence;

    public SequenceParser(String sequence) {
        this.sequence = sequence;
    }

    /**
     * Returns the map boundaries in X, Y format.
     *
     * @return int[2] {x, y}
     * @throws MissionException raises an exception if map boundaries could not be decoded
     */
    public int[] getMapXandY() throws MissionException {
        Matcher matcher = MAP_PATTERN.matcher(this.sequence);
        if (matcher.find()) {
            String[] mapBounds = matcher.group(MAP_GROUP).split(" ");
            return new int[]{Integer.parseInt(mapBounds[0]), Integer.parseInt(mapBounds[1])};
        }

        throw new MissionException("Could not obtain map bounds");
    }

    /**
     * Parse and returns a list representation of the robot part of the sequence.
     *
     * @return a list of robot sequences
     */
    public List<RobotSequence> getRobotSequences() {
        List<RobotSequence> robotSequences = new ArrayList<>();
        Matcher matcher = ROBOT_PATTERN.matcher((this.sequence));
        while (matcher.find()) {
            robotSequences.add(parseRobotSequence(matcher));
        }

        return robotSequences;
    }

    /**
     * Parses the robot sequence. As the regex has named groups, it's quite easy to wire
     * the things up.
     *
     * @param matcher a regex matcher
     * @return a RobotSequence representation of the input string
     */
    private RobotSequence parseRobotSequence(Matcher matcher) {
        String[] positions = matcher.group(POSITION_GROUP).split(" ");
        String commands = matcher.group(COMMANDS_GROUP);
        Position position = new Position(
                Integer.parseInt(positions[0]),
                Integer.parseInt(positions[1]));

        Cardinal cardinal = null;
        if (Objects.nonNull(matcher.group(HEADING_GROUP))) {
            cardinal = Cardinal.fromAbbreviation(matcher.group(HEADING_GROUP));
        }
        return new RobotSequence(position, cardinal, commands);
    }
}
