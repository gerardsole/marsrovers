package com.newrelic.marsrovers.logic.exceptions;

public class MissionException extends Exception {
    public MissionException(String message) {
        super(message);
    }
}
